const express = require('express');
const bodyParser = require('body-parser')
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const compression = require('compression')
mongoose.Promise = global.Promise;
const path = require('path');
const postRouter = require('./server/routes/postRouter');
const userRouter = require('./server/routes/userRouter');
const teamRouter = require('./server/routes/teamRouter');
const videoRouter = require('./server/routes/videoRouter');
const tourGamesRouter = require('./server/routes/tourGamesRouter');
const itemRouter = require('./server/routes/itemRouter');
const aboutRouter = require('./server/routes/aboutRouter');
let oneYear = 1 * 365 * 24 * 60 * 60 * 1000;


mongoose.connect('mongodb://hayfanUser:hayfanPass@mongodb1.webrahost.eu:27017/hayfan', {
  useNewUrlParser: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("db success")
  // we're connected!
});


dotenv.config();
const app = express();

// app.use(express.static('uploads'));
app.use(compression());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection at:', p, 'reason:', reason);
})
    .on('uncaughtException', (err) => {
      console.log(err);
      console.error(`Caught exception: \n ${err}`);
    })
    .on('ECONNREFUSED', (err) => {
      console.error('ECONNREFUSED: \n', err);
    })
    .on('ELIFECYCLE', (err) => {
      console.error('ELIFECYCLE: \n', err);
    });

app.use('/', express.static(path.join(__dirname, 'uploads'),{ maxAge: oneYear }))
// app.use(express.static(path.join(__dirname, "/dist")));


app.all("/*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});
app.use('/api/posts', postRouter);
app.use('/api/users', userRouter);
app.use('/api/teams', teamRouter);
app.use('/api/videos', videoRouter);
app.use('/api/tourGames', tourGamesRouter);
app.use('/api/shop', itemRouter);
app.use('/api/about',aboutRouter);
// app.get('*',function(req,res){
//   res.sendFile(path.join(__dirname,"/dist/index.html"),{ maxAge: oneYear });
// });
app.use((err, req, res, next) => {
  if (err) {
    res.status(err.statusCode || err.status || 500).send(err.data || err.message || {});
  } else {
    next();
  }
});
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// const port = process.env.PORT || 51233;
const port = 51233;

app.listen(port, () => {
  console.log(`Server successfully running on port ${port}`)
});
