const mongoose = require('mongoose');
const Videos = require('../models/videoModel');

const { Schema } = mongoose;
const postModelSchema = mongoose.Schema({
  title: {
    type: String,

  },
  description: {
    type: String,

  },
  firstLeague: {
    type: Boolean,
    default: false,

  },
  highestLeague: {
    type: Boolean,
    default: false,
  },
  nationalLeague: {
    type: Boolean,
    default: false,
  },
  otherLeague: {
    type: Boolean,
    default: false,
  },
  images: {
    type: [],
  },
  videos: {
    type: [{ type: Schema.Types.ObjectId, ref: 'Videos' }],

  },
  status: {
    type: Boolean,
    default: false,

  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now,
  },
  updated_at: {
    type: Date,
  },
});

const post = mongoose.model('posts', postModelSchema);


module.exports = post;
