const mongoose = require('mongoose');

const TeamSchema = mongoose.Schema({
  name: {
    type: String,

  },
  points: {
    type: Number,
    default: 0,
  },
  games: {
    type: Number,
    default: 0,
  },
  image: {
    type: String,
  },
  firstLeague: {
    type: Boolean,
    default: false,
  },
  premierLeague: {
    type: Boolean,
    default: false,
  },

});
module.exports = mongoose.model('Teams', TeamSchema);
