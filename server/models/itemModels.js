const mongoose = require('mongoose');

const itemModelSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
  },
  sizeS: {
    type: Boolean,
    default: true,
  },
  sizeM: {
    type: Boolean,
    default: true,
  },
  sizeL: {
    type: Boolean,
    default: true,
  },
  images: {
    type: [],
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now,
  },
  updated_at: {
    type: Date,
  },
});

const item = mongoose.model('items', itemModelSchema);


module.exports = item;
