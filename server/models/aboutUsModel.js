const mongoose = require('mongoose');

const aboutUsModelSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  email: {
    type: String,

  },
  address: {
    type: String,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now,
  },

});

const about = mongoose.model('about', aboutUsModelSchema);


module.exports = about;
