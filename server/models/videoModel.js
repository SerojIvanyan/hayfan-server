const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const videoModelSchema = mongoose.Schema({
  title: {
    type: String,

  },
  path: {
    type: String,
    required: true,

  },
  type: {
    type: Boolean,
    default: false,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now,
  },
  updated_at: {
    type: Date,
  },


});

module.exports = mongoose.model('Videos', videoModelSchema);
