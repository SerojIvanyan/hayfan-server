const mongoose = require('mongoose');
const Teams = require('./teamModels');

const { Schema } = mongoose;
const TourGamesSchema = mongoose.Schema({
  teamFirstId: {
    type: Schema.Types.ObjectId,
    ref: 'Teams',
  },
  teamSecondId: {
    type: Schema.Types.ObjectId,
    ref: 'Teams',
  },
  teamFirstScore: {
    type: Number,

  },
  ended: {
    type: Boolean,
    default: false,
  },
  teamSecondScore: {
    type: Number,
  },
  firstLeague: {
    type: Boolean,
    default: false,
  },
  premierLeague: {
    type: Boolean,
    default: false,
  },
  gameDate: {
    type: Date,
    default: null,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now,
  },


});
module.exports = mongoose.model('TourGames', TourGamesSchema);
