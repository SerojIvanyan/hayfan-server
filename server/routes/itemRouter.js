const express = require('express');

const Router = express.Router();
const path = require('path');
const fs = require('fs');

const jsonPath = path.join(__dirname, '..', '..', 'uploads');

const multer = require('multer');
const crypto = require('crypto');
const verifyToken = require('../auth/verifyToken');
const itemModel = require('../models/itemModels');

let imgPath;
const imgArr = [];

const storage = multer.diskStorage({
  destination(req, file, cb) {
    if (!fs.existsSync(jsonPath)) {
      fs.mkdirSync('uploads');
      fs.mkdirSync(`${jsonPath}/postPictures`);
      fs.mkdirSync(`${jsonPath}/teamMembersPictures`);
      fs.mkdirSync(`${jsonPath}/shopItemPicture`);
    }
    imgArr.length = 0;
    cb(null, process.env.SHOPITEMPICTUREDEST);
  },
  filename(req, file, cb) {
    crypto.pseudoRandomBytes(8, (err, raw) => {
      imgPath = raw.toString('hex') + file.originalname;
      imgArr.push(imgPath);
      if (err) return cb(err);
      cb(null, imgPath);
    });
  },
});
const upload = multer({
  storage,
});

Router.get('/:skip', (req, res) => {
  const perPage = 2;
  const page = req.params.skip || 1;
  itemModel.find({}, (err, posts) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(posts);
  }).sort([
    ['_id', -1],
  ]).skip((perPage * page) - perPage);
});

Router.post('/create', verifyToken, upload.array('myFile'), (req, res) => {
  const obj = JSON.parse(req.body.data);
  const body = {
    title: obj.title,
    description: obj.description,
    images: obj.images,
    price: obj.price,
    sizeS: obj.sizeS,
    sizeM: obj.sizeM,
    sizeL: obj.sizeL,
    images: [],
    created_at: Date.now(),
  };

  body.images = imgArr;
  const newItem = new itemModel(body);
  newItem.save()
    .then((posts) => {
      imgArr.length = 0;
      res.send(posts);
    });
});


Router.put('/update/:id', verifyToken, upload.array('myFile'), (req, res) => {
  const obj = JSON.parse(req.body.data);
  obj.updated_at = Date.now();
  console.log('body', obj);
  if(imgPath) {

    itemModel.findById(req.params.id, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (fs.existsSync(`${jsonPath}/shopItemPicture/${data.images[0]}`)) {

        fs.unlinkSync(`${jsonPath}/shopItemPicture/${data.images[0]}`);

        data.images = [imgPath];
        data.save((err, res) => {
          if (err) {
            return res.send(err)
          }
        })
      }
    })
  }
  itemModel.findByIdAndUpdate(req.params.id, obj, {
    new: true,
  }, (err, updatedPost) => {
    if (err) {
      res.send(err);
    }
    res.send(updatedPost);
  });
});

Router.delete('/delete/:id', verifyToken, async (req, res) => {
  itemModel.findByIdAndDelete(req.params.id)
    .then((item) => {
      console.log(item);
      if (fs.existsSync(`${jsonPath}/shopItemPicture/${item.images[0]}`)) {
        fs.unlink(`${jsonPath}/shopItemPicture/${item.images[0]}`, (err) => {
          if (err) return console.log(err);
          console.log('file deleted successfully');
          res.send({
            status: 1,
          });
        });
      }
    })
    .catch((err) => {
      res.send(err);
    });
});


module.exports = Router;
