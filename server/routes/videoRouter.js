const express = require('express');

const Router = express.Router();
const verifyToken = require('../auth/verifyToken');

const videoModel = require('../models/videoModel');


Router.get('/toped', (req, res) => {
  videoModel.find({ type: true }, (err, videos) => {
    if (err) {
      res.send(err);
    }
    res.send(videos);
  }).sort([['_id', -1]]);
});
Router.get('/:skip', (req, res) => {
  const perPage = 2;
  const page = req.params.skip || 1;
  videoModel.find({}, (err, videos) => {
    if (err) {
      res.send(err);
    }
    res.send(videos);
  }).sort([['_id', -1]])
    .skip((perPage * page) - perPage)
    .limit(perPage);
});
Router.post('/create', verifyToken, (req, res) => {
  const body = {
    title: req.body.title,
    path: req.body.path,
    type: req.body.type,
    created_at: Date.now(),
  };
  const video = new videoModel(body);
  video.save((err, data) => {
    if (err) {
      res.send(err);
    }
    res.send(data);
  });
});
Router.put('/update/:id', verifyToken, (req, res) => {
  const body = {
    title: req.body.title,
    path: req.body.path,
    type: req.body.type,
    updated_at: Date.now(),
  };
  videoModel.findByIdAndUpdate(req.params.id, body, { new: true }, (err, updVideo) => {
    if (err) {
      res.send(err);
    }
    res.send(updVideo);
  });
});
Router.delete('/delete/:id', verifyToken, (req, res) => {
  videoModel.findByIdAndRemove(req.params.id, (err, delVideo) => {
    if (err) {
      res.send(err);
    }
    res.send({ status: 1 });
  });
});


module.exports = Router;
