const express = require('express');

const Router = express.Router();
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const verifyToken = require('../auth/verifyToken');

const jsonPath = path.join(__dirname, '..', '..', 'uploads');

const crypto = require('crypto');
const videoModel = require('../models/videoModel');
const postModel = require('../models/postModels');

let imgPath;
const imgArr = [];
const storage = multer.diskStorage({
  destination(req, file, cb) {
    if (!fs.existsSync(jsonPath)) {
      fs.mkdirSync('uploads');
      fs.mkdirSync(`${jsonPath}/postPictures`);
      fs.mkdirSync(`${jsonPath}/teamMembersPictures`);
      fs.mkdirSync(`${jsonPath}/shopItemPicture`);
    }
    imgArr.length = 0;
    cb(null, process.env.POSTPICTURESDEST);
  },
  filename(req, file, cb) {
    crypto.pseudoRandomBytes(8, (err, raw) => {
      imgPath = raw.toString('hex') + file.originalname;
      imgArr.push(imgPath);
      if (err) return cb(err);
      cb(null, imgPath);
    });
  },
});
const upload = multer({
  storage,
});

Router.get('/:skip', (req, res) => {
  const perPage = 4;
  const page = req.params.skip || 1;
  postModel.find({}, (err, posts) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(posts);
  }).sort([
    ['_id', -1],
  ]).skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos');
});

Router.get('/id/:id', (req, res) => {
  postModel.findById(req.params.id, (err, posts) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(posts);
  }).populate('videos', 'path');
});


Router.get('/first-league/:skip', (req, res) => {
  const perPage = 4;
  const page = req.params.skip || 1;

  postModel.find({
    firstLeague: true,
  })
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos')
    .sort([
      ['_id', -1],
    ])
    .then((posts) => {
      res.send(posts);
    })
    .catch((err) => res.send(err));
});

Router.get('/premier-league/:skip', (req, res) => {
  console.log('premi');
  const perPage = 4;
  const page = req.params.skip || 1;
  console.log(req.params.skip);
  postModel.find({
    highestLeague: true,
  })
    .sort([
      ['_id', -1],
    ])
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos')
    .then((posts) => {
      res.send(posts);
    })
    .catch((err) => res.send(err));
});

Router.get('/national-team/:skip', (req, res) => {
  const perPage = 4;
  const page = req.params.skip || 1;
  postModel.find({
    nationalLeague: true,
  })
    .sort([
      ['_id', -1],
    ])
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos')

    .then((posts) => {
      res.send(posts);
    })
    .catch((err) => res.send(err));
});

Router.get('/other/:skip', (req, res) => {
  const perPage = 4;
  const page = req.params.skip || 1;
  postModel.find({
    otherLeague: true,
  })
    .sort([
      ['_id', -1],
    ])
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos')
    .then((posts) => {
      res.send(posts);
    })
    .catch((err) => res.send(err));
});


Router.get('/getTopedPosts', (req, res) => {
  const perPage = 4;
  const page = req.params.skip || 1;
  postModel.find({
    status: true,
  })
    .sort([
      ['_id', -1],
    ])
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('videos')
    .then((posts) => {
      res.send(posts);
    })
    .catch((err) => res.send(err));
});
Router.post('/create', verifyToken, upload.array('myFile'), (req, res) => {
  const obj = JSON.parse(req.body.data);
  const body = {
    title: obj.title,
    description: obj.description,
    highestLeague: obj.highestLeague,
    firstLeague: obj.firstLeague,
    nationalLeague: obj.nationalLeague,
    otherLeague: obj.otherLeague,
    status: obj.status,
    videos: [],
    images: [],
    created_at: Date.now(),
  };

  const videosArray = (obj.videos && obj.videos.length && obj.videos) || [];
  if (videosArray.length) {
    const prom = new Promise((res, rej) => {
      videoModel.insertMany(videosArray)
        .then((videos) => {
          videos.forEach((video) => {
            body.videos.push(
              video.id,
            );
          });
          res(videos);
        })
        .catch((err) => {
          rej(err);
        });
    });
    prom.then(() => {
      body.images = imgArr;
      const newPost = new postModel(body);
      newPost.save()
        .then((post) => {
          imgArr.length = 0;
          imgPath = undefined;
          res.send(post);
        })
        .catch((err) => {
          res.send(err);
        });
    });
  } else {
    body.images = imgArr;
    const newPost = new postModel(body);
    newPost.save()
      .then((posts) => {
        imgArr.length = 0;
        imgPath = undefined;
        res.send(posts);
      });
  }
});


Router.put('/update/:id', verifyToken, upload.array('myFile'), async (req, res, next) => {
  try {
    const obj = JSON.parse(req.body.data);
    obj.updated_at = Date.now();
    console.log('imgPath', imgPath);

    const oldPost = await postModel.findById(req.params.id);
    if (imgPath) {
      if (fs.existsSync(`${jsonPath}/postPictures/${oldPost.images[0]}`)) {
        fs.unlinkSync(`${jsonPath}/postPictures/${oldPost.images[0]}`);
      }
      obj.images = [imgPath];
      await oldPost.update(obj, { new: true, upsert: true });
      await oldPost.save();
    }
    if (!imgPath) {
      await oldPost.update(obj, { new: true, upsert: true });
      await oldPost.save();
    }
    // if(){}
    const videosArray = (obj.videos && obj.videos.length && obj.videos) || [];
    if (videosArray.length) {
      await videoModel.findByIdAndRemove(videosArray[0]['_id']);
      let prom =new Promise((resp, rej) => {
        videoModel.insertMany(videosArray)
          .then((videos) => {
            videos.forEach((video) => {
              oldPost.videos = [
                video.id,
              ];
              oldPost.save().then().catch((e) => next(e));
            });
          })
          .catch((err) => {
            rej(err);
          });
      });
    }
    const result = await postModel.findById(req.params.id);

    imgPath = undefined;
    return res.send({ result });
  } catch (e) {
    return next(e);
  }
  // const obj = JSON.parse(req.body.data);
  //
  // obj.updated_at = Date.now();
  // if (imgPath) {
  //   postModel.findById(req.params.id, (err, data) => {
  //     if (err) {
  //       res.send(err);
  //     }
  //     if (fs.existsSync(`${jsonPath}/postPictures/${data.images[0]}`)) {
  //       fs.unlinkSync(`${jsonPath}/postPictures/${data.images[0]}`);
  //       console.log(data.images);
  //       data.images = [imgPath];
  //       console.log(data.images);
  //       data.save((error, result) => {
  //         if (err) {
  //           res.send(err);
  //         }
  //         data.update(obj, (error1, result1) => {
  //           if (error1) {
  //             return error1;
  //           }
  //           res.send({ result1 });
  //         });
  //       });
  //     }
  //   });
  // }
  //
  // postModel.findByIdAndUpdate(req.params.id, obj, {
  //   new: true,
  //   upsert: true,
  // }, (err, updatedPost) => {
  //   if (err) {
  //     res.send(err);
  //   }
  //   console.log(updatedPost);
  //   res.send(updatedPost);
  // });
});

Router.delete('/delete/:id', async (req, res) => {
  postModel.findByIdAndDelete(req.params.id)
    .then((post) => {
      videoModel.deleteMany({
        _id: {
          $in: post.videos[0],
        },
      })
        .then(() => {
          if (fs.existsSync(`${jsonPath}/postPictures/${post.images[0]}`)) {
            fs.unlink(`${jsonPath}/postPictures/${post.images[0]}`, (err) => {
              if (err) return err;
              console.log('file deleted successfully');
            });
          }
          res.send({
            status: 1,
          });
        });
    })
    .catch((err) => {
      res.send(err);
    });
});


module.exports = Router;
