const express = require('express');

const Router = express.Router();
const verifyToken = require('../auth/verifyToken');


const tourGamesModel = require('../models/tourGamesModel');

Router.get('/', (req, res) => {
  tourGamesModel.find({}, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).populate('teamFirstId').populate('teamSecondId').sort([
    ['_id', -1]]);
});
Router.get('/getNotStartedTourGame/premier-league', (req, res) => {
  tourGamesModel.find({
    ended: false,
    premierLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).populate('teamFirstId').populate('teamSecondId').sort([
    ['_id', -1],
  ])
    .limit(5);
});
Router.get('/getNotStartedTourGame/first-league', (req, res) => {
  tourGamesModel.find({
    ended: false,
    firstLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).populate('teamFirstId').populate('teamSecondId').sort([
    ['_id', -1],
  ])
    .limit(5);
});
Router.get('/getLastEndedTourGame/premier-league', (req, res) => {
  tourGamesModel.find({
    ended: true,
    premierLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).populate('teamFirstId').populate('teamSecondId').sort([
    ['_id', -1],
  ])
    .limit(5);
});
Router.get('/getLastEndedTourGame/first-league', (req, res) => {
  tourGamesModel.find({
    ended: true,
    firstLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).populate('teamFirstId').populate('teamSecondId').sort([
    ['_id', -1],
  ])
    .limit(5);
});


Router.post('/create', (req, res) => {
  const body = {
    teamFirstId: req.body.teamFirstId,
    teamSecondId: req.body.teamSecondId,
    teamFirstScore: req.body.teamFirstScore,
    teamSecondScore: req.body.teamSecondScore,
    tourName: req.body.tourName,
    ended: req.body.ended,
    gameDate: req.body.date,
    premierLeague: req.body.premierLeague,
    firstLeague: req.body.firstLeague,
    created_at: Date.now(),
  };
  const tourGame = new tourGamesModel(body);
  tourGame.save((err, respon) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    tourGamesModel.findById(respon._id, (err, teams) => {
      if (err) {
        console.log(err);
        res.send(err);
      }
      res.send(teams);
    }).populate('teamFirstId').populate('teamSecondId');
  });
});
Router.put('/update/:id', verifyToken, (req, res) => {
  const { body } = req;
  tourGamesModel.findByIdAndUpdate(req.params.id, body, {
    new: true,
  })
    .then((updttour) => res.send(updttour))
    .catch((err) => res.send(err));
});
Router.delete('/delete/:id', verifyToken, async (req, res) => {
  tourGamesModel.findByIdAndDelete(req.params.id)
    .then((tour) => {
      res.send({
        status: 1,
      });
    })

    .catch((err) => {
      res.send(err);
    });
});


module.exports = Router;
