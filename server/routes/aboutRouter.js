const express = require('express');

const Router = express.Router();
const verifyToken = require('../auth/verifyToken');


const aboutUsModel = require('../models/aboutUsModel');

Router.get('/', (req, res) => {
  aboutUsModel.find({}, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).sort([
    ['_id', -1],
  ]);
});

Router.post('/create', verifyToken, (req, res) => {
  const body = {
    title: req.body.title,
    description: req.body.description,
    phone: req.body.phone,
    email: req.body.email,
    address: req.body.address,
    created_at: Date.now(),
  };
  const about = new aboutUsModel(body);
  aboutUsModel.deleteMany({}).then(() => {
    about.save((err, respon) => {
      if (err) {
        console.log(err);
        res.send(err);
      }

      res.send(respon);
    });
  }).catch((e) => {
    console.log(e);
    res.send(e);
  });
});
Router.put('/update/:id', verifyToken, (req, res) => {
  const { body } = req;
  aboutUsModel.findByIdAndUpdate(req.params.id, body, {
    new: true,
  })
    .then((updtteam) => res.send(updtteam))
    .catch((err) => res.send(err));
});
Router.delete('/delete/:id', verifyToken, async (req, res) => {
  aboutUsModel.findByIdAndDelete(req.params.id)
    .then((team) => {
      res.send({
        status: 1,
      });
    })

    .catch((err) => {
      res.send(err);
    });
});
module.exports = Router;
