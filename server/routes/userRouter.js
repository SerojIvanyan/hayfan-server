const express = require('express');

const Router = express.Router();

const jwt = require('jsonwebtoken');
const userModel = require('../models/userModel');
const config = require('../auth/config');


const VerifyToken = require('../auth/verifyToken');

Router.get('/', (req, res) => {
  userModel.find({}, (err, users) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(users);
  });
});
Router.get('/verifyToken',VerifyToken ,(req, res) => {
res.send({auth:true,msg: "Everything is ok.Token checked"})
});
Router.get('/logout', (req, res) => {
  res.status(200).send({ auth: false, token: null });
});

Router.post('/create', VerifyToken, (req, res) => {
  console.log(1111);
  const body = {
    username: req.body.username,
    password: req.body.password,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    age: req.body.age,
    permissions: req.body.permissions,
    created_at: Date.now(),

  };
  const newUser = new userModel(body);
  newUser.save((err, user) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    const token = jwt.sign({ id: user._id, permission: user.permissions }, config.secret, {
      expiresIn: 7200,
    });

    res.status(200).send({ auth: true, token });
    res.send(user);
  });
});

Router.post('/login', (req, res) => {
  // attempt to authenticate user
  userModel.getAuthenticated(req.body.username, req.body.password, (err, user, reason) => {
    if (err) return res.status(500).send('Error on the server.');
    // login was successful if we have a user

    if (user) {
      // handle login success
      const token = jwt.sign({ id: user._id, permission: user.permissions }, config.secret, {
        expiresIn: '7d', // expires in 7 day
      });

      res.status(200).send({ auth: true, token });
      return;
    }

    // otherwise we can determine why we failed
    const reasons = userModel.failedLogin;

    switch (reason) {
      case reasons.NOT_FOUND: res.status(404).send('No user found.');
      case reasons.PASSWORD_INCORRECT: res.status(401).send({ auth: false, token: null });
        // note: these cases are usually treated the same - don't tell
        // the user *why* the login failed, only that it did
        break;
      case reasons.MAX_ATTEMPTS:
        res.send({ message: 'Your account is temporarily locked ' });
        // send email or otherwise notify user that account is
        // temporarily locked
        break;
    }
  });
});


Router.get('/me', VerifyToken, (req, res, next) => {
  userModel.findById(req.userId, { password: 0 }, (err, user) => {
    if (err) return res.status(500).send('There was a problem finding the user.');
    if (!user) return res.status(404).send('No user found.');

    res.status(200).send(user);
  });
});

// add the middleware function
Router.use((user, req, res, next) => {
  res.status(200).send(user);
});


Router.put('/update', VerifyToken, (req, res) => {
  const { body } = req;
  body.updated_at = Date.now();
  userModel.findByIdAndUpdate(req.userId, body, { new: true }, (err, updatedUser) => {
    if (err) {
      res.send(err);
    }
    res.send(updatedUser);
  });
});

Router.delete('/delete/:id', VerifyToken, (req, res) => {
  userModel.findByIdAndRemove(req.params.id, (err, deletedUser) => {
    if (err) {
      res.send(err);
    }
    res.send({ status: 0 });
  });
});


module.exports = Router;
