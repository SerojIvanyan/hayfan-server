const express = require('express');

const Router = express.Router();
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const multer = require('multer');
const verifyToken = require('../auth/verifyToken');

const TeamModel = require('../models/teamModels');
const TourGamesModel = require('../models/tourGamesModel');


const jsonPath = path.join(__dirname, '..', '..', 'uploads');
let imgPath;

const storage = multer.diskStorage({
  destination(req, file, cb) {
    if (!fs.existsSync(jsonPath)) {
      fs.mkdirSync('uploads');
      fs.mkdirSync(`${jsonPath}/postPictures`);
      fs.mkdirSync(`${jsonPath}/teamMembersPictures`);
      fs.mkdirSync(`${jsonPath}/shopItemPicture`);
    }
    cb(null, process.env.TEAMPICTURESDEST);
  },
  filename(req, file, cb) {
    crypto.pseudoRandomBytes(8, (err, raw) => {
      imgPath = raw.toString('hex') + file.originalname;
      if (err) return cb(err);
      return cb(null, imgPath);
    });
  },
});
const upload = multer({
  storage,
});

Router.get('/premier-league', (req, res) => {
  TeamModel.find({
    premierLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).sort([
    ['points', -1],
    ['games', 1],
  ]);
});
Router.get('/first-league', (req, res) => {
  TeamModel.find({
    firstLeague: true,
  }, (err, teams) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.send(teams);
  }).sort([
    ['points', -1],
    ['games', 1],
  ]);
});
Router.post('/create', verifyToken, upload.array('myFile'), (req, res) => {
  const obj = JSON.parse(req.body.data);
  const body = {
    name: obj.name,
    games: obj.games,
    image: imgPath,
    points: obj.points,
    firstLeague: obj.firstLeague,
    premierLeague: obj.premierLeague,
  };
  const team = new TeamModel(body);
  team.save((err, respon) => {
    if (err) {
      console.log(err);
      res.send(err);
    }

    res.send(respon);
  });
});
Router.put('/update/:id', verifyToken, upload.array('myFile'), (req, res) => {
  const obj = JSON.parse(req.body.data);

  console.log('body', obj);
  if(imgPath){

    TeamModel.findById(req.params.id,(err,data)=>{
      if (err){
        res.send(err)
      }
      if (fs.existsSync(`${jsonPath}/teamMembersPictures/${data.image}`)) {

        fs.unlinkSync(`${jsonPath}/teamMembersPictures/${data.image}`);

        data.image=imgPath;
        data.save((err,res)=>{
          if (err){
           return res.send(err)
          }
        })
      }
    })

  }
  TeamModel.findByIdAndUpdate(req.params.id, obj, {
    new: true,
  })
    .then((updtteam) => res.send(updtteam))
    .catch((err) => res.send(err));
});
Router.delete('/delete/:id', verifyToken, async (req, res) => {
  console.log(`${jsonPath}/teamMembersPictures/`);

  TeamModel.findByIdAndDelete(req.params.id)
    .then((team) => {
      if (team) {
        TourGamesModel.deleteMany({ $or: [{ teamFirstId: req.params.id }, { teamSecondId: req.params.id }] }).then((resu) => {
          if (fs.existsSync(`${jsonPath}/teamMembersPictures/${team.image}`)) {
            fs.unlink(`${jsonPath}/teamMembersPictures/${team.image}`, (err) => {
              if (err) return console.log(err);
              console.log('file deleted successfully');
              res.send({
                status: 1,
              });
            });
          }
          res.send({
            status: 1,
          });
        });
        // tourGamesModel.deleteMany({$or: [{'teamFirstId': req.params.id}, {'teamSecondId': req.params.id}]})
      }
      res.send({
        status: 1,
      });
    })

    .catch((err) => {
      res.send(err);
    });
});
module.exports = Router;
