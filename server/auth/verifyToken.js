const jwt = require('jsonwebtoken');
const config = require('./config');

function verifyToken(req, res, next) {
  if (req.headers.authorization) {
    var token = req.headers.authorization.split('e$e$e$ ')[1];
  } else {
    return res.status(403).send({ auth: false, message: 'No token provided.' });
  }

  if (!token) {
    return res.status(403).send({ auth: false, message: 'No token provided.' });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      console.log(err);
      return res.status(500).send({ auth: false, message: err.message });
    }

    // if everything good, save to request for use in other routes
    const now = Date.now().valueOf() / 1000;


    if (typeof decoded.exp !== 'undefined' && decoded.exp < now) {
      return res.status(500).send({ auth: false, message: 'Token expired please log in.' });
    }
    next();
  });
}
module.exports = verifyToken;
